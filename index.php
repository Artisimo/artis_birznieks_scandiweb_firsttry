<?php
    require_once 'autoload.php';
    $conn = connectDB::connect();
    
    if(isset($_POST['delete'])){
    $chkarr = $_POST['checkbox'];
    foreach($chkarr as $id){
        $conn -> query("DELETE FROM products WHERE id = '$id'");
    }
    header("Location:index.php");
}
?>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="css/indexStyle.css">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Product List</title>
</head>
<body>
<div class="header" id ="myHeader">
    <h2>Product list</h2>
    <button name="delete" type="submit" form="products" id="delete-product-btn" >MASS DELETE</button>
    <button onclick="redirectToAddProduct();" id = "add-btn" >ADD</button>
</div>
<hr>
<div id = "container">
    <form method = "post" id = "products">
        <?php
        $query = "SELECT * FROM products";
        $result = $conn->query($query);
        echo "<table>";
        
        while($row = mysqli_fetch_array($result))
        {
        echo "<tr>";
        ?>
            <td><p><input class = "delete-checkbox"  type="checkbox" name="checkbox[]" value="<?php echo $row['id']; ?>"><br>
            <?php echo $row["SKU"]?><br>
            <?php echo $row["name"]?><br>
            <?php echo $row["price"]. " $"?><br>
            <?php echo $row["product_specific_attribute"]?></p></td>
        <?php
        echo "</tr>";
        }
        echo "</table>";
        ?>
    </form>
</div>
<hr>
<footer>
    <p class= "footer_text">Scandiweb Test Assignment</p>
</footer>
<?php
mysqli_close($conn);
?>
</body>
<script>
function redirectToAddProduct() {
  location.replace("https://artisbirznieks.000webhostapp.com/addproduct.php");
}
</script>
</html>