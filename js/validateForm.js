function validateForm() {
    var price = document.forms["product_form"]["price"].value;
    var sku = document.forms["product_form"]["sku"].value;
    var name = document.forms["product_form"]["name"].value;
    var form = document.getElementById("product_form");
    var errorBox = document.getElementById("errorMessages");
    
    var inputs = form.getElementsByTagName("input");
    var allFields = inputs.length;
    var nrFields = allFields - 2;
    errorBox.style.textAlign = "center";
    errorBox.style.color = "red";

    for(var i = 0; i < allFields; i++) {
        input = inputs[i];
        if(input.value == "") {
            errorBox.innerText = "Please, submit required data";
            errorBox.style.display = "Block";
            return false;
        }
    }
    for(i = 2; i < allFields; i++){
        if(isNaN(inputs[i].value)){
            errorBox.innerText = "Please, provide the data of indicated type";
            errorBox.style.display = "Block";
            return false;
        }
    }
}