<?php
class DVD extends product{
    public $size;

    function __construct(){
        $this -> name = $_POST['name'];
        $this -> SKU = $_POST['sku'];
        $this -> price= $_POST['price'];
        $this -> size = $_POST['size'];
        $this -> product_specific_attribute = "Size: " . $this->size . " MB";
    }

    public function validateData(){
        if(!$this -> isString(trim(htmlspecialchars($this->SKU)))){
            return false;
        }
        if(!$this -> isString(trim(htmlspecialchars($this->name)))){
            return false;
        }
        if(!$this -> isInt(trim(htmlspecialchars($this-> size)))){
            return false;
        }
        if(!$this -> isString(trim(htmlspecialchars($this->product_specific_attribute)))){
            return false;
        }
        if(!$this -> isFloat(trim(htmlspecialchars($this->price)))){
            return false;
        }
        return true;
    }
}
?>