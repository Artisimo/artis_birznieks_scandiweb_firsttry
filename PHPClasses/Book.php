<?php
class Book extends product{
    public $weight;
    function __construct(){
        $this -> name = $_POST['name'];
        $this -> SKU = $_POST['sku'];
        $this -> price= $_POST['price'];
        $this -> weight = $_POST['weight'];
        $this -> product_specific_attribute = "Weight: " . $this->weight. " KG";
    }

    public function validateData(){
        if(!$this -> isString(trim(htmlspecialchars($this->SKU)))){
            return false;
        }
        if(!$this -> isString(trim(htmlspecialchars($this->name)))){
            return false;
        }
        if(!$this -> isInt(trim(htmlspecialchars($this-> weight)))){
            return false;
        }
        if(!$this -> isString(trim(htmlspecialchars($this->product_specific_attribute)))){
            return false;
        }
        if(!$this -> isFloat(trim(htmlspecialchars($this->price)))){
            return false;
        }
        return true;
    }
}
?>