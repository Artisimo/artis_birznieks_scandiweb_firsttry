<?php

abstract class product implements validateData{

    public $SKU;
    public $name;
    public $price;
    public $product_specific_attribute;
  
    public function addToDB(){
        $sql = "INSERT INTO products(SKU, name, price, product_specific_attribute) VALUES('$this->SKU', '$this->name', '$this->price', '$this->product_specific_attribute');";
        
        $conn = connectDB::connect();
        
        if($conn -> query($sql)){
            $conn -> close();
        }
    }
    public function isInt($val) {
		$val = filter_var($val, FILTER_VALIDATE_INT);
		if ($val === false) {
			return false;
		}else{
            return true;
        }
	}
    public function isString($val) {
		if (!is_String($val)) {
			return false;
		}else{
            return true;
        }
	}
    public function isFloat($val){
        $val = filter_var($val, FILTER_VALIDATE_FLOAT);
		if ($val === false) {
			return false;
		}else{
            return true;
        }
    }
    public function validateData(){
        if(!$this -> isString(trim(htmlspecialchars($this->sku)))){
            return false;
        }
        if(!$this -> isString(trim(htmlspecialchars($this->name)))){
            return false;
        }
        if(!$this -> isString(trim(htmlspecialchars($this->product_specific_attribute)))){
            return false;
        }
        if(!$this -> isFloat(trim(htmlspecialchars($this->price)))){
            return false;
        }
        return true;
    }
}