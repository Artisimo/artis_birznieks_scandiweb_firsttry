<?php
interface validateData{
    public function isInt($val);
    public function isString($val);
    public function isFloat($val);
}
?>