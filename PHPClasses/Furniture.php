<?php
class Furniture extends product{
    public $height;
    public $width;
    public $length;

    function __construct(){
        $this -> name = $_POST['name'];
        $this -> SKU = $_POST['sku'];
        $this -> price= $_POST['price'];
        $this -> height = $_POST['height'];
        $this -> width = $_POST['width'];
        $this -> length = $_POST['length'];
        $this -> product_specific_attribute = $this->height . " x " . $this->width . " x " . $this->length;
    }

    public function validateData(){
        if(!$this -> isString(trim(htmlspecialchars($this->SKU)))){
            return false;
        }
        if(!$this -> isString(trim(htmlspecialchars($this->name)))){
            return false;
        }
        if(!$this -> isInt(trim(htmlspecialchars($this-> height)))){
            return false;
        }
        if(!$this -> isInt(trim(htmlspecialchars($this-> width)))){
            return false;
        }
        if(!$this -> isInt(trim(htmlspecialchars($this-> length)))){
            return false;
        }
        if(!$this -> isString(trim(htmlspecialchars($this->product_specific_attribute)))){
            return false;
        }
        if(!$this -> isFloat(trim(htmlspecialchars($this->price)))){
            return false;
        }
        return true;
    }
}
?>