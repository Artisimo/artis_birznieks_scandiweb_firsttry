<?php
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    require_once 'autoload.php';
    $class = $_POST['productType'];
    $product = new $class();
    if($product -> validateData()){ 
        $product -> addToDB();
    }
    header("Location: https://artisbirznieks.000webhostapp.com/");
}
?>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="css/addProductStyle.css">
<title>Add a Product!</title>
<script src="js/validateForm.js"></script>
</head>
<body>
<div id ="btnDIV">
    <h2>Product Add</h2>
    <button class = "button" onclick="redirect();" id = "cancel-btn" >CANCEL</button>
    <input class = "button" type="submit" form="product_form" value = "Save"></input>
</div>
<hr>
<div class = "form_container">
    <form method = "post" action = "" id = "product_form" onsubmit="return validateForm();">
        <label for="sku">SKU:</label>
        <input type="text" id="sku" name="sku"><br>
        <label for="name">Name:</label>
        <input type="text" id="name" name="name"><br>
        <label for="price">Price ($):</label>
        <input type="text" id="price" name="price"><br>
        <label for="productType">Type Switcher</label>
        <select id="productType" name= "productType" onchange="show();">
        <option value="DVD">DVD</option>
        <option value="Book">Book</option>
        <option value="Furniture">Furniture</option>
        </select>
        <div id="dynamicForm" style ="display:none"></div>
        <div id="errorMessages" style = "display:none"></div>   
    </form>
</div>
<hr>
<footer>
    <p class= "footer_text">Scandiweb Test Assignment</p>
</footer>
</body>
<script type="module" src="js/showElementProperty.js"></script>
<script src = "js/redirect.js"></script>
</html>